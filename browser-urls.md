# Browser URLs

- `/`
- `/customers/:customerId/`
- `/customers/:customerId/contracts/:contractId/`
- `/customers/:customerId/contracts/:contractId/?list={cases,activityLogs,payments,assets}`
- `/contracts/:contractId/cases/`

## /

| Key name | Key type | Description |
| -------- | -------- | ----------- |
| `search` | `string` | TODO        |
