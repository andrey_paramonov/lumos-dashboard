# Lumos requests

```ts
interface Fields {
  // TODO
  [fieldName: string]: string | number | boolean; // �
}

interface UniqueResource {
  id: string;
  fields: Fields;
}

interface Customer extends UniqueResource {
  name: string;
}

interface Contract extends UniqueResource {
  name: string;
}

interface SpsWait {
    pending: true;
    timeout: number; // seconds
}

interface SpsResult {
    pending: false;
    sps: Sps;
}

interface Sps extends UniqueResource {
  name: string;
  missingDays: number;
  inLightDays: number;
  seniorityDays: number;
  freeUseDays: number;
}

interface Asset extends UniqueResource {}

interface Case extends UniqueResource {}

interface ActivityLog extends UniqueResource {}

interface Payment extends UniqueResource {}
```

## Resource requests

| url                           | response body         |
| ----------------------------- | --------------------- |
| `/customers/:id/`             | `Customer`            |
| `/customers/:id/contracts`    | `Contract[]`          |
| `/contracts/:id/`             | `Contract`            |
| `/contracts/:id/sps   `       | `SpsWait | SpsResult` |
| `/contracts/:id/assets`       | `Asset[]`             |
| `/contracts/:id/cases`        | `Case[]`              |
| `/contracts/:id/activityLogs` | `ActivityLog[]`       |
| `/contracts/:id/payments`     | `Payment[]`           |
| `/assets/:id/`                | `Asset`               |
| `/cases/:id/`                 | `Case`                |
| `/activityLogs/:id/`          | `ActivityLog`         |
| `/payments/:id/`              | `Payment`             |

## Methods requests

```ts
interface LoginParams {
  username: string;
  password: string;
}

interface LoginResult {
  name: string;
  permissions: {
    /* TODO */
  };
}

interface LabelsResult {
  [labelName: string]: string;
}

interface FieldInfo {
  label: string;
  name: string;
  type: 'string' | 'date' | '�'; // TODO
}

interface SearchParams {
  value: string;
}

interface SearchResultItem {
  contractId: string;
  customerId: string;
  customerName: string;
  createdDate: string; // ISO 8601
  phoneNumber: string;
  spsId: string;
  active: boolean;
}

interface FieldSetResult {
  [objectName: string]: FieldInfo[];
}
```

| url                          | url params     | response body        |
| ---------------------------- | -------------- | -------------------- |
| `/signIn`                    | `LoginParams`  | `SignInResult`       |
| `/signOut`                   | `{}`           | TODO                 |
| `/labels`                    | `{}`           | `LabelsResult`       |
| `/setting/portalPreferences` | `{}`           | TODO                 |
| `/setting/pickListValues`    | `{}`           | TODO                 |
| `/setting/fieldSet`          | `{}`           | `FieldSetResult`     |
| `/search`                    | `SearchParams` | `SearchResultItem[]` |
