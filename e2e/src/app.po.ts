import {browser, by, element, promise} from 'protractor';

export class AppPage {
  // TODO: indicate type
  public navigateTo(): promise.Promise<any> {
    return browser.get('/');
  }

  // TODO: indicate type
  public getParagraphText(): any {
    return element(by.css('lumos-root h1')).getText();
  }
}
