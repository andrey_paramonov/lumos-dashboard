export enum EColors {
  GREEN = '#58B93F',
  YELLOW = '#f8cf1c',
  RED = '#ff2600',
  DARKGREY = '#C5CDE1',
  LIGHTGREY = '#edf0f6',
  BLUE = '#0000ff',
  CUSTOMBLACK = '#303030'
}