import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {LabelsMockupService} from '../mockups/labels.mockup';
import {DashboardMockupService} from '../mockups/dashboard.mockup';

let _host = '';
const LS = localStorage.getItem('requestUrl');
if (LS) {
  _host = LS;
}

const API_PREFIX: string = _host+'/services/apexrest';

@Injectable()
export class ServerService {

  public headers: HttpHeaders = new HttpHeaders();
  public _sessionId: string = '';

  public constructor(
    private _httpClient: HttpClient, 
    private _labelsMock: LabelsMockupService,
    private _dashMock: DashboardMockupService) {
  }


  public updateHeaders(): void {

    let _sessionId = '';
    let headersValues: any = {};
    const sessionId = localStorage.getItem('sessionId');
    if (sessionId) {
      _sessionId = 'Bearer '+sessionId;
      this._sessionId = 'Bearer '+sessionId;
    }

    if (environment.production) {
      headersValues['Authorization'] = _sessionId;
    }

    this.headers = new HttpHeaders(headersValues);
  }



  public _request(type: string, URL:string, data:any, callback?:any) {

    this.updateHeaders();

    let _headerValues: any = { 
      'Content-Type': 'application/json'
    };
    if (environment.production) {
      _headerValues['Authorization'] = this._sessionId;
    }

    let _headers = new HttpHeaders(_headerValues);

    if ('post' === type) { 
      this._httpClient.post( URL, data, {headers: _headers} )
      .subscribe(
        (data: any) => {
          if ( callback ) {
            callback( data );
          }
        },
        (err: any) => {
          if ( callback ) {
            callback( err );
          }
        }
      );
    }

    if ('patch' === type) {
      this._httpClient.patch(URL, data, {headers: _headers})
      .subscribe(
        (data: any) => {
          if ( callback ) {
            callback( data );
          }
        },
        (err: any) => {
          if ( callback ) {
            callback( err );
          }
        }
      );
    }

  }

  public postRequest(URL:string, data:any, callback?:any) {
    this._request('post', URL, data, callback);
  }
  public patchRequest(URL:string, data:any, callback?:any) {
    this._request('patch', URL, data, callback);
  }

  public getRequest ( URL:string, callback?:any ) {
    this.updateHeaders();

    let _headerValues: any = { 
      'Content-Type': 'application/json'
    };
    if (environment.production) {
      _headerValues['Authorization'] = this._sessionId;
    }

      return this._httpClient.get(URL, {headers: _headerValues})
      .subscribe(
        (data: any) => {
          if ( callback ) {
            callback( data );
          }
        },
        (err: any) => {
          if ( callback ) {
            callback( err );
          }
        }
      );
  }


/*==============================*\
             Dashboard
\*==============================*/

  public getDashboardDaily(contractId: string, spsId: string, callback?: any) {

    if (environment.production) {

      this.getRequest(`${API_PREFIX}/contracts/${contractId}/sps/${spsId}/telemetry/daily`, (data: any)=> {
        if (callback && data.data) {
          try {
            callback(JSON.parse(data.data));
          } catch(e) {
            console.log(e);
          }
        }
      });

    } else {
      setTimeout(() => {
        let _data = this._dashMock.getDaily();
        if (callback) {
          callback(_data);
        }
      }, 750);
    }

  }

  public postDashboardDaily(contractId: string, spsId: string, callback: any) {


    if (environment.production) {
      this.postRequest(
        `${API_PREFIX}/contracts/${contractId}/sps/${spsId}/telemetry/daily`, 
        {}, 
        (response: any) => {
          console.log('%cPost Daily', 'text-decoration:underline;color:purple');
          callback(response);
        });
    } else {
      console.log('%cPost Today Request imitation', 'color:#7a7a7a;font-style:italic;');
      setTimeout(()=>{
        // let _mockReqCount: number = ~~(Math.random()*3);
        let _mockReqCount: number = 1;
        let _response;
        if (_mockReqCount == 0) {
          _response = {
            data: {
              execution_status: 'Some Error'
            }
          }
        } else {
          _response = {
            data: {
              execution_status: 'Success'
            }
          }
        }
        callback(_response);
      }, 750);
    }

  }

  public getDashboardSnapshot(timestamp: string, contractId: string, spsId: string, callback?: any) {

    if (environment.production) {

      this.getRequest(`${API_PREFIX}/contracts/${contractId}/sps/${spsId}/telemetry/snapshot?dateStart=${timestamp}`, (data: any)=> {
        if (callback && data.data) {
          try {
            callback(JSON.parse(data.data));
          } catch(e) {
            console.log(e);
          }
        }
      });

    } else {
      let _data = this._dashMock.getSnapshot();
      if (callback) {
        callback(_data);
      }
    }
  }

  public postDashboardSnapshot(contractId: string, spsId: string, callback: any) {


    if (environment.production) {
      this.postRequest(
        `${API_PREFIX}/contracts/${contractId}/sps/${spsId}/telemetry/snapshot`, 
        {}, 
        (response: any) => {
          console.log(response);
          console.log('%cPost Snapshot', 'text-decoration:underline;color:purple');
          callback(response);
        });
    } else {
      console.log('%cPost Snapshot Request imitation', 'color:#7a7a7a;font-style:italic;');
      setTimeout(()=>{
        // let _mockReqCount: number = ~~(Math.random()*3);
        let _response;
        // if (_mockReqCount == 0) {
        //   _response = {
        //     data: {
        //       execution_status: 'Some Error'
        //     }
        //   }
        // }
        // if (_mockReqCount == 1) {
          // _response = {
          //   data: '<html>↵<head><title>503 Service Temporarily Unavailable</title></head>↵<body bgcolor="white">↵<center><h1>503 Service Temporarily Unavailable</h1></center>↵</body>↵</html>↵',
          //   status: 503
          // }
        // } else {
          _response = {
            data: {
              execution_status: 'Success'
            }
          }
        // }
        callback(_response);
      }, 750);
    }

  }


  public getTelemetrySettings(callback: any) {

    this.getRequest(`${API_PREFIX}/telemetry/settings`, (data: {tlmList: Array<any>, status: string})=> {
      callback(data);
    });

  }




  public getTelemetryStateSettings(callback: any) {

    this.getRequest(`${API_PREFIX}/telemetry/states`, (data: {tlmList: Array<any>, status: string})=> {
      callback(data);
    });

  }




  public getLabelsData(): Observable<any> {

    // if (environment.production) {

    //   this.updateHeaders();

    //   let _headerValues: any = { 
    //     'Content-Type': 'application/json'
    //   };
    //   if (environment.production) {
    //     _headerValues['Authorization'] = this._sessionId;
    //   }

    //   let data: Array<string> = [
    //     'telemetry_Panel',
    //     'telemetry_Daily_average_health',
    //     'telemetry_Health',
    //     'telemetry_No_data',
    //     'telemetry_Good',
    //     'telemetry_Bad',
    //     'telemetry_Battery',
    //     'telemetry_Battery_level',
    //     'telemetry_Current_status',
    //     'telemetry_Sunrise',
    //     'telemetry_Sunset',
    //     'telemetry_Max',
    //     'telemetry_Charge_state',
    //     'telemetry_Daily_average_charge_state',
    //     'telemetry_Daily_average_load',
    //     'telemetry_Reception',
    //     'telemetry_Daily_average_reception',
    //     'telemetry_without_reception'
    //   ];

    //   let _headers = new HttpHeaders(_headerValues);

    //   return this._httpClient.post<any>(`${API_PREFIX}/telemetry/settings`, data, {headers: _headers});
    // } else {
      return of(this._labelsMock.labels);
    // }
  }


  public getDetails(contractId: string, spsId: string, callback: any): void {


    if (environment.production) {
      this.getRequest(
        `${API_PREFIX}/contracts/${contractId}/sps/${spsId}/telemetry/details`, 
        (response: any) => {
          callback(response);
        }); 

    } else {
      setTimeout(() => {
        let data: any = this._dashMock.getDetails();
        callback(data);
      }, 750);
    }
  }

}
