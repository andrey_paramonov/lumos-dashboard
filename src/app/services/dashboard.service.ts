/*
********************************************************************
********************************************************************
***    Dashboard service for recieving and manipulating
***                    SPS data
********************************************************************
********************************************************************
*/


import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Router, Event, NavigationEnd} from '@angular/router';
import {ServerService} from './server.service';
import {AdditionalService} from './additional.service';
// import {HelperService} from './helper.service';
import {WidgetSettings} from '../classes/widget-settings.class';
import {EColors} from '../enums/colors.enums';
import {environment} from '../../environments/environment';
import {ISunPanelHealth, ISunPanelStatus} from '../interfaces/widgets/sun-panel-health.interface';
import {IDTParams} from '../interfaces/dashboard-telemetry-parameters.interface';
import {ITimeDifference} from '../interfaces/time-differenct.interface';
import {TelemetryStateSettingsMockupService} from '../mockups/telemetry-state.mockup';
import {LabelsMockupService} from '../mockups/labels.mockup';


// **********************************
// ****  Definition of additional
// ****  interface for data
// **********************************
interface IDWSettings {
  dailyData: null | any;
  snapshotData: null | any;
}





// **********************************
// ****  Adding to global window
// ****  telemetry settings
// **********************************
declare global {
  interface Window { 
    telemetryStateSettings: any;
    $Label: any;
  }
}





// **********************************
// ****  Class definition
// **********************************
@Injectable()
export class DashboardService {

  public dashboardData: Subject<IDWSettings> = new Subject();
  public dashboardWidgetSettings: Subject<WidgetSettings> = new Subject();
  public onDSInit: Subject<void> = new Subject();

  public dailyTelemetryParams: Subject<IDTParams> = new Subject();
  public snapshotTelemetryParams: Subject<IDTParams> = new Subject();

  public responseErrorsData: Subject<{daily:string, snapshot:string}> = new Subject();
  public leftCreditBalanceData: Subject<string> = new Subject();
  private _RED: {daily: string, snapshot: string} = {
    daily: '',
    snapshot: ''
  };

  private _labelsMock: undefined | any;

  private _dws: IDWSettings = {
    dailyData: null,
    snapshotData: null
  };

  private _telemetryPotentialPowers: Array<any> = [];
  private _initializationSettings: any = {
    hasTelemetrySettings: false,
    serviceInit: false
  };

  private _contractId: string = '';
  private _spsId: string = '';

  private REQUESTS_AMOUNT: number = 2;
  private REQUEST_TIMEOUT: number = 60;

  private _pollingTime__daily: number = 0;
  private _pollingTime__snapshot: number = 0;

  private _pollingTimeout__daily: any;
  private _pollingTimeout__snapshot: any;
  private IS_INTERRUPTED_POST__DAILY: boolean = false;
  private IS_INTERRUPTED_POST__SNAPSHOT: boolean = false;

  private _pollingStartTime_snapshot: Date = new Date();

  constructor(
    private lblsSrv: LabelsMockupService,
    private model: ServerService,
    private router: Router,
    private _additionalService: AdditionalService,
    // private _helper: HelperService,
    private _telemetryMock: TelemetryStateSettingsMockupService) {

    this._initialization();
  }



  // **********************************
  // ****  Default Initialization
  // **********************************
  private _initialization(): void {
    this._setTelemetrySettings();
    this._setLabels();
    this.model.getTelemetryStateSettings((data: { tlmList: Array<any>, status: string }) => {
      window.telemetryStateSettings = data.tlmList;
      this.model.getTelemetrySettings((data: { tlmList: Array<any>, status: string }) => {
        this.telemetryPotentialPowers = data.tlmList;
        this._setInitSetting('hasTelemetrySettings', true);
        this._checkInitialization();
      });
    });
    this.serviceInit();

  }






  // **********************************
  // ****  Set up Labels
  // **********************************
  private _setLabels() {
    this._labelsMock = this.lblsSrv.labels;
    // if (environment.production) {
    //   if (!window.telemetryStateSettings) {
    //     window.telemetryStateSettings = [];
    //   }
    // }
  }





  // **********************************
  // ****  Set up custom settings
  // **********************************
  private _setTelemetrySettings() {
    if (environment.production) {
      if (!window.telemetryStateSettings) {
        window.telemetryStateSettings = [];
      }
    } else {
      window.telemetryStateSettings = this._telemetryMock.telemetryStateSettings;
    }
  }




  // **********************************
  // ****  Checking if all service 
  // ****  functionality and data
  // ****  has been loaded and 
  // ****      initialized
  // **********************************
  private _checkInitialization(): void {
    let hasFalseSteps: boolean = false;
    for (let key in this._initializationSettings) {
      if (!this._initializationSettings[key]) {
        hasFalseSteps = true;
        break;
      }
    }
    if (!hasFalseSteps) {
      this.DSStart();
    }
  }





  // **********************************
  // ****  Set up pre-init parameters 
  // **********************************
  private _setInitSetting(parameter: string, value: boolean): void {
    if (this._initializationSettings.hasOwnProperty(parameter)) {
      this._initializationSettings[parameter] = value;
    }
  }








  // **********************************
  // ****  Some defaults preparation 
  // ****     of Initialization
  // **********************************
  public serviceInit(): void {
    this._subscribeCurrentRouteParameters();
    this.setDashboardData(this._dws);
    this._setInitSetting('serviceInit', true);
    this._checkInitialization();
  }







  // **********************************
  // ****  Subscribing current
  // ****  router params
  // **********************************
  private _subscribeCurrentRouteParameters(): void {
    this.router.events.subscribe((val: Event) => {
      if (val instanceof NavigationEnd) {
        // this.contractId = this._helper.getURLParameter('contractId') || '';
        // this.spsId = this._helper.getURLParameter('spsId') || '';
        this.contractId = localStorage.getItem('contractId') || '';
        this.spsId = localStorage.getItem('spsId') || '';

        this._additionalService.initialization(this.contractId, this.spsId);

        console.log("%cDashboard service ~ contract ID: "+this.contractId, 'font-size:85%;font-style:italic;color:#51519a;');
        console.log("%cDashboard service ~ sps ID: "+this.spsId, 'font-size:85%;font-style:italic;color:#51519a;');
      }
    });
  }




  // **********************************
  // ****  Subscribing dashboard 
  // ****    initializaion
  // **********************************
  public get onDSStart(): Observable<void> {
    return this.onDSInit.asObservable();
  }



  // **********************************
  // ****  Start Dashboard init
  // **********************************
  public DSStart(): void {
    this.onDSInit.next();
  }





  // **********************************
  // ****  Subscribing dashboard 
  // ****  data by observable
  // **********************************
  public get getDashboardData(): Observable<IDWSettings> {
    return this.dashboardData.asObservable();
  }







  // **********************************
  // ****  set up dashboard data 
  // ****  by observable
  // **********************************
  public setDashboardData(dashboardData: IDWSettings): void {
    this.dashboardData.next(dashboardData);
  }






  // **********************************
  // ****  Subscribing widgets 
  // ****  settings by observable
  // **********************************
  public get getDashboardWidgetSettings(): Observable<WidgetSettings> {
    return this.dashboardWidgetSettings.asObservable();
  }






  // **********************************
  // ****  set up widgets 
  // ****  settings by observable
  // **********************************
  public setDashboardWidgetSettings(widgetSettings: WidgetSettings): void {
    this.dashboardWidgetSettings.next(widgetSettings);
  }






  // **********************************
  // ****  get dashboard daily data
  // **********************************
  public getDashboardDaily(): void {
    this.model.getDashboardDaily(this._contractId, this._spsId, (dashboardData: any) => {
      this.dashboardData.next(dashboardData);
      console.log('%cDaily', 'color:blue;text-decoration:underline;');
      console.log(dashboardData);
      this._dws.dailyData = dashboardData.daily_telemetry_records || null;
      this.setDashboardData(this._dws);
    });
  }





  // **********************************
  // ****  sending post to update
  // ****  daily data
  // **********************************
  public postDashboardDaily(): void {
    this._RED.daily = '';
    this.responseErrorsData.next(this._RED);
    this.model.postDashboardDaily(this._contractId, this._spsId, (response: any) => {

      if (!response.data) {
        if (response.type === 'danger') {
          try {
            this.IS_INTERRUPTED_POST__DAILY = true;
            if (response.msg) {
              this._RED.daily = response.msg;
              this.responseErrorsData.next(this._RED);
            }
          } catch (exception) {
            this._RED.daily = 'Uknown error has been occured';
            this.responseErrorsData.next(this._RED);
          }
        }
        return;
      }

      if (typeof(response.data) === 'string') {
        response.data = JSON.parse(response.data);
      }

      if (response.data.execution_status && response.data.execution_status.toLowerCase() == 'success') {
        this.getDashboardDaily();
        this.IS_INTERRUPTED_POST__DAILY = true;
      }


      if (response.data && response.status === 503) {
        this.IS_INTERRUPTED_POST__DAILY = true;
        this._RED.daily = response.data;
        this.responseErrorsData.next(this._RED);
      }
      if (response.status === 502 || response.status === 501 || response.status === 500) {
        try {
          this.IS_INTERRUPTED_POST__DAILY = true;
          let _parsedData: any = JSON.parse(response.data);
          if (_parsedData.code) {
            this._RED.daily = _parsedData.code;
            this.responseErrorsData.next(this._RED);
          }
        } catch (exception) {
          this._RED.daily = 'Uknown error has been occured';
          this.responseErrorsData.next(this._RED);
        }
      }
      // this.dashboardData.next(dashboardData);    
    });
  }





  // **********************************
  // ****  get dashboard 
  // ****  snapshot data
  // **********************************
  public getDashboardSnapshot(currentTimestamp?: undefined | Date): void {
    var requestTimestamp: Date = new Date();
    if (currentTimestamp !== undefined) {
      requestTimestamp = currentTimestamp;
    }
    this.model.getDashboardSnapshot(requestTimestamp.toISOString(), this._contractId, this._spsId, (dashboardData: any) => {
      this.dashboardData.next(dashboardData);
      console.log('%cSnapshot', 'color:red;text-decoration:underline;');
      console.log(dashboardData);
      this._dws.snapshotData = dashboardData.snapshot_telemetry_records || null;
      this._updateSnapshotAdditionalDetails(this._dws.snapshotData);
      this.setDashboardData(this._dws);
    });
  }


  private _updateSnapshotAdditionalDetails(snapshotData: null | any): void {
    let IDU: string = '';
    if (snapshotData && snapshotData.length) {
        IDU = 'HW'+snapshotData[0]['snapshot_telemetry']['idu_hardware_version']+'SW'+snapshotData[0]['snapshot_telemetry']['idu_software_version']; 
    }
    this._additionalService.setIDU(IDU);
  }


  // **********************************
  // ****  sending post to update
  // ****  snapshot data
  // **********************************
  public postDashboardSnapshot(): void {
    this._RED.snapshot = '';
    this.responseErrorsData.next(this._RED);
    this.model.postDashboardSnapshot(this._contractId, this._spsId, (response: any) => {
      if (!response.data) {
        if (response.type === 'danger') {
          try {
            this.IS_INTERRUPTED_POST__SNAPSHOT = true;
            if (response.msg) {
              this._RED.snapshot = response.msg;
              this.responseErrorsData.next(this._RED);
            }
          } catch (exception) {
            this._RED.snapshot = 'Uknown error has been occured';
            this.responseErrorsData.next(this._RED);
          }
        }
        return;
      }

      if (typeof(response.data) === 'string') {
        response.data = JSON.parse(response.data);
      }
      if (response.data.execution_status && response.data.execution_status.toLowerCase() == 'success') {
        this.getDashboardSnapshot(this._pollingStartTime_snapshot);
        this.IS_INTERRUPTED_POST__SNAPSHOT = true;
      }


      if (response.status === 503) {
        this.IS_INTERRUPTED_POST__SNAPSHOT = true;
        this._RED.snapshot = response.data;
        this.responseErrorsData.next(this._RED);
      }
      if (response.status === 502 || response.status === 501 || response.status === 500) {
        try {
          this.IS_INTERRUPTED_POST__SNAPSHOT = true;
          let _parsedData: any = JSON.parse(response.data);
          if (_parsedData.code) {
            this._RED.snapshot = _parsedData.code;
            this.responseErrorsData.next(this._RED);
          }
        } catch (exception) {
          this._RED.snapshot = 'Uknown error has been occured';
          this.responseErrorsData.next(this._RED);
        }
      }
      // this.dashboardData.next(dashboardData);    
    });
  }





  // **********************************
  // ****  Getting current dashaboard
  // ****  settings
  // **********************************
  public getDashboardSettings(callback: any): void {
    let _response: string | WidgetSettings = '';
    callback(_response);
  }



  // **********************************
  // ****  ContractId setter
  // **********************************
  public set contractId(contractId: string) {
    this._contractId = contractId;
  }


  // **********************************
  // ****  ContractId getter
  // **********************************
  public get contractId(): string {
    return this._contractId;
  }


  // **********************************
  // ****  SpsId setter
  // **********************************
  public set spsId(spsId: string) {
    this._spsId = spsId;
  }


  // **********************************
  // ****  SpsId getter
  // **********************************
  public get spsId(): string {
    return this._spsId;
  }





  /* ******************************************************************** *\

                  Dashboard widgets logic block

  |* ******************************************************************** */


  public getWidgetData(fieldModel: string, data: any): WidgetSettings {
    let _widgetSettings: WidgetSettings = new WidgetSettings();
    if (data.data) {
      _widgetSettings.sunPanelHealth = this._getPanelDetails(fieldModel, data.data);
      _widgetSettings.dataLoaded = true;
      _widgetSettings.reception = this.getReceptionColor(fieldModel, data.data);
      _widgetSettings.currentLoad = this.getCurrentLoad(fieldModel, data.data);

      let chargeState: any = this.getChargeState(fieldModel, data.data);
      _widgetSettings.batteryCharge.charge_state = chargeState.charge_state;
      if (chargeState.battery_current) {
        _widgetSettings.batteryCharge.battery_current = chargeState.battery_current;
      } else {
        _widgetSettings.batteryCharge.battery_sunrise = chargeState.battery_sunrise;
        _widgetSettings.batteryCharge.battery_sunset = chargeState.battery_sunset;
        _widgetSettings.batteryCharge.battery_max = chargeState.battery_max;
      }

      _widgetSettings.connectionStatus = {
        isConnectionStable: this._isPanelConnected(fieldModel, data.data),
        advise: ''
      };
    }
    this.updateLeftCredit(fieldModel, data);
    return _widgetSettings;
  }



  // **********************************
  // ****  Comparsion of today
  // ****  and selected date
  // **********************************
  public isTodayDate(datetimeString: string): boolean {
    let inputDate = new Date(datetimeString);
    let todaysDate = new Date();
    return inputDate.getUTCFullYear() == todaysDate.getUTCFullYear() &&
      inputDate.getUTCMonth() == todaysDate.getUTCMonth() &&
      inputDate.getUTCDate() == todaysDate.getUTCDate();
  }






  // **********************************
  // ****  Telemetry potential powers 
  // ****        setter
  // **********************************
  public set telemetryPotentialPowers(telemetryPotentialPowers: Array<any>) {
    this._telemetryPotentialPowers = telemetryPotentialPowers;
  }




  // **********************************
  // ****  Telemetry potential powers 
  // ****        getter
  // **********************************
  public get telemetryPotentialPowers(): Array<any> {
    return this._telemetryPotentialPowers;
  }



  /*  <—————————————————————————————————————————————————> *\
                    Start Panel Health
  \*  <—————————————————————————————————————————————————> */


  private _getPanelDetails(fieldModel: string, data: any) {
    let panelDetails: ISunPanelHealth = {
      status: '',
      power_value: '',
      description: ''
    };
    if (data) {
      if (fieldModel == 'daily') {
        let accumulative_power = this._getSumOfGeneratedPower(data.data_packets);
        let panel_potential_power = this._getPanelPotentialPower('', this.isTodayDate(data.telemetry_time_stamp));
        let details = this._getNormalizedPowerLevel(accumulative_power, panel_potential_power, fieldModel);
        panelDetails.status = details.status;
        panelDetails.description = details.description;
        panelDetails.power_value = accumulative_power + 'Wh';
      } else {
        let panel_power = data.snapshot_telemetry.volt_panel * data.snapshot_telemetry.current_panel;
        let panel_potential_power = this._getPanelPotentialPower(data.snapshot_telemetry.receivedAtTime, false);
        let details = this._getNormalizedPowerLevel(panel_power, panel_potential_power, fieldModel);
        panelDetails.status = details.status;
        panelDetails.description = details.description;
        panelDetails.power_value = Math.round(panel_power) + 'W';
      }
    }
    return panelDetails;
  }

  private _getSumOfGeneratedPower(packets: Array<any>): number {
    let sum: number = 0;
    packets.forEach((packet: any) => {
      sum += packet.panel_generated_power;
    });
    return sum;
  }

  private _getPanelPotentialPower(receivedAtTime: string, isToday: boolean) {
    let panelPotentialPower: number = 0;
    let potentialPowerList = this.telemetryPotentialPowers;
    if (potentialPowerList === undefined || potentialPowerList == []) {
      return 0;
    }
    if (receivedAtTime) {
      let utcHours = new Date(receivedAtTime).getHours();
      let utcMinutes = new Date(receivedAtTime).getMinutes();
      let res = utcHours + utcMinutes / 100;
      for (let i = 0; i < potentialPowerList.length; i++) {
        if (potentialPowerList[i].Start_Time__c < res && res < potentialPowerList[i].End_Time__c) {
          panelPotentialPower = potentialPowerList[i].Potential_Power__c;
        }
      }
      return panelPotentialPower;
    } else {
      if (isToday) {
        panelPotentialPower = this._getSumOfPotentialPowerToday(potentialPowerList);
      } else {
        panelPotentialPower = this._getPanelPotentialPowerDefault(potentialPowerList);
      }
    }

    return panelPotentialPower;
  }

  private _getSumOfPotentialPowerToday(potentialPowerList: Array<any>): number {
    let sum: number = 0;
    let minHour = 6;
    let maxHour = new Date().getHours();
    potentialPowerList.forEach((potentialPowerItem: any) => {
      if (potentialPowerItem.Start_Time__c >= minHour && potentialPowerItem.End_Time__c < maxHour) {
        sum += potentialPowerItem.Potential_Power__c;
      }
    });
    return sum;
  }

  private _getPanelPotentialPowerDefault(potentialPowerList: Array<any>): number {
    let potentialPower: number = 0;
    potentialPowerList.forEach((potentialPowerItem: any) => {
      if (potentialPowerItem.Start_Time__c == 0 && potentialPowerItem.End_Time__c == 0) {
        potentialPower = potentialPowerItem.Potential_Power__c;
      }
    });
    return potentialPower;
  }

  private _getNormalizedPowerLevel(panelPower: number, panelPotentialPower: number, fieldModel: string) {
    let powerPercent: number = panelPower * 100 / panelPotentialPower;
    let res: { status: ISunPanelStatus, description: string } = { status: 'bad', description: 'Poor' };
    if (fieldModel === 'daily') {
      if (powerPercent >= 85) {
        res = { status: 'good', description: 'Good' }
      } else if (70 <= powerPercent && powerPercent < 85) {
        res = { status: 'medium', description: 'Medium' }
      } else if (powerPercent < 70) {
        res = { status: 'bad', description: 'Poor' }
      }
    } else {
      if (panelPower >= panelPotentialPower || powerPercent > 100) {
        res = { status: 'good', description: 'Good' }
      } else if (70 <= powerPercent && powerPercent < 100) {
        res = { status: 'medium', description: 'Medium' }
      } else if (powerPercent < 70) {
        res = { status: 'bad', description: 'Poor' }
      }
    }
    return res;
  }

  private _isPanelConnected(fieldModel: string, data: any) {
    if (fieldModel == 'snapshot') {
      let panel_id: string = data.snapshot_telemetry.panel_id;
      if (panel_id && panel_id.toLowerCase() != "null" && !this._panelIdContainsOnlyZeros(panel_id)) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  private _panelIdContainsOnlyZeros(panelId: string) {
    for (let i = 0; i < panelId.length; i++) {
      if (panelId[i] != '0') {
        return false;
      }
    }
    return true;
  }


  /*  <—————————————————————————————————————————————————> *\
                    End Panel Health
  \*  <—————————————————————————————————————————————————> */


  /*  <—————————————————————————————————————————————————> *\
                    Start Charge State
  \*  <—————————————————————————————————————————————————> */


  public getChargeState(fieldModel: string, data: any) {
    let result: any = {};
    if (fieldModel == 'snapshot') {
      result = {
        charge_state: this._getChargeStateByType('snapshot', data),
        battery_current: this._getButteryCurrent(data)
      };
    } else {
      result = {
        charge_state: this._getChargeStateByType('daily', data),
        battery_sunrise: this._getSocSunrise(data.data_packets),
        battery_sunset: this._getSocSunset(data.data_packets),
        battery_max: this._getSocMaxDaily(data.data_packets)
      };
    }
    return result;
  }


  private _getButteryCurrent(data: any) {
    let result = {
      battery_level: -1,
      description: 'none',
      soc: 0
    };
    let soc = data.snapshot_telemetry.nominal_state_of_charge;
    result.battery_level = Math.round(soc * 6 / 100);
    result.description = soc + '%';
    result.soc = soc;
    return result;
  }

  private _getSocMaxDaily(packets: Array<any>) {
    let result = {
      battery_level: -1,
      description: 'none',
      soc: 0
    };
    let packet = this._getPacketForMaxDaily(packets);
    if (packet) {
      result.battery_level = Math.round(packet.state_of_charge * 6 / 100);
      result.description = packet.state_of_charge + '%';
    }
    if (result.battery_level == 0) {
      result.battery_level = 1;
    }
    return result;
  }


  private _getPacketForMaxDaily(packets: Array<any>) {
    let packet: any | undefined = undefined;
    for (let _packet of packets) {
      if (this._getValidPacketsFromTelemetry(_packet)) {
        if (!packet || packet['state_of_charge'] < _packet['state_of_charge']) {
          packet = _packet;
        }
      }
    }
    return packet;
  }

  private _getSocSunrise(packets: Array<any>) {
    let result = {
      battery_level: -1,
      description: 'none',
      packet_time: '',
      soc: 0
    };
    let packet = this._getSocSunrisePackets(packets);
    if (packet) {
      result.battery_level = Math.round(packet.state_of_charge * 6 / 100);
      if (result.battery_level == 0) {
        result.battery_level = 1;
      }
      result.description = packet.state_of_charge + '%';
    }
    return result;
  }


  private _getSocSunrisePackets(packets: Array<any>) {
    let packet: any | undefined = undefined;
    for (let _packet of packets) {
      if ((new Date(_packet.start_time)).getUTCHours() == 6) {
        packet = _packet;
      }
    }
    return packet;
  }


  private _getSocSunset(packets: any) {
    let result = {
      battery_level: -1,
      description: 'none',
      packet_time: '',
      soc: 0
    };
    let packet = this._getSocSunsetPackets(packets);
    if (packet) {
      result.battery_level = Math.round(packet.state_of_charge * 6 / 100);
      if (result.battery_level == 0) {
        result.battery_level = 1;
      }
      result.description = packet.state_of_charge + '%';
    }
    return result;
  }


  private _getSocSunsetPackets(packets: Array<any>) {
    let packet: any | undefined = undefined;
    for (let _packet of packets) {
      if ((new Date(_packet.start_time)).getUTCHours() == 18) {
        packet = _packet;
      }
    }
    return packet;
  }


  private _getChargeStateByType(type: string, data: any) {

    let charge = (type == 'snapshot') ?
      data.snapshot_telemetry.current_battery * data.snapshot_telemetry.volt_battery :
      data.avg_charge_vs_consumption;
    let stateItem = {
      State__c: 0,
      Is_charging__c: true,
      Is_steady__c: false,
      Description__c: 'Invalid data'
    }
    if (!!window.telemetryStateSettings) {
      let stateItemF = this._getStateByType_find(charge, type, 'chargeState');
      if (stateItemF) {
        stateItem = stateItemF;
      }
    }
    return {
      charge_level: stateItem.State__c,
      is_charging: stateItem.Is_charging__c,
      is_steady: stateItem.Is_steady__c,
      description: (this._labelsMock ? this._labelsMock['telemetry_' + (stateItem.Description__c ? stateItem.Description__c.split(' ').join('_') : '')] : '')
    };

  }


  private _getStateByType_find(_value: number, type: string, filterName: string) {
    let stateItemF: any | undefined = undefined;
    let filteredStates = this._getChargeStateByType_filter(type, filterName);
    let _state;
    let rule1: boolean = false;
    let rule2: boolean = false;
    let rule3: boolean = false;
    for (_state of filteredStates) {
      rule1 = _state.hasOwnProperty('Min_value__c') &&
        _state.hasOwnProperty('Max_value__c') &&
        _value >= _state.Min_value__c &&
        _value < _state.Max_value__c;
      rule2 = _state.hasOwnProperty('Min_value__c') &&
        !_state.hasOwnProperty('Max_value__c') &&
        _value >= _state.Min_value__c;
      rule3 = !_state.hasOwnProperty('Min_value__c') &&
        _state.hasOwnProperty('Max_value__c') &&
        _value < _state.Max_value__c;
      if (rule1 || rule2 || rule3) {
        stateItemF = _state;
      }
    }

    return stateItemF;
  }



  private _getChargeStateByType_filter(type: string, searching: string) {
    let _customTelemetrySettings: Array<any> = [];
    let telemetrySetting;

    for (telemetrySetting of window.telemetryStateSettings) {
      if (telemetrySetting.Name.indexOf(searching) != -1 && telemetrySetting.Type__c === type) {
        _customTelemetrySettings.push(telemetrySetting);
      }
    }
    return _customTelemetrySettings;
  }


  private _getValidPacketsFromTelemetry(packet: any) {
    let start: Date | number = new Date(packet.start_time);
    let end: Date | number = new Date(packet.end_time);
    start = start.getUTCHours();
    end = end.getUTCHours();
    return (start >= 6 && end <= 18);
  }



  public getBatteryPartColor(batteryPart: number, batteryLevel: number): string {
    let color: string = EColors.LIGHTGREY;
    if (batteryLevel == 1 && batteryPart == 0) {
      color = EColors.RED;
    } else if (batteryLevel == 2 && batteryPart < 2 || batteryLevel == 3 && batteryPart < 3 || batteryLevel == 4 && batteryPart < 4) {
      color = EColors.YELLOW;
    } else if (batteryLevel == 5 && batteryPart < 5 || batteryLevel == 6 && batteryPart < 6) {
      color = EColors.GREEN;
    }
    return color;
  };


  /*  <—————————————————————————————————————————————————> *\
                    End Charge State
  \*  <—————————————————————————————————————————————————> */




  /*  <—————————————————————————————————————————————————> *\
                    Start Current Load
  \*  <—————————————————————————————————————————————————> */


  public getCurrentLoad(fieldModel: string, data: any) {
    let result = {
      state: 0,
      arrowDeg: 0,
      fill: EColors.LIGHTGREY,
      description: 'Overload',
      value: '0W'
    };

    let loadPower: number = 0;
    let data_packets: Array<any> = [];
    let stateItem;

    if (data) {
      result.fill = EColors.CUSTOMBLACK;
      if (fieldModel == 'daily') {
        data_packets = [];
        for (let _packet of data.data_packets) {
          if (this._getValidPacketsFromTelemetry(_packet)) {
            data_packets.push(_packet);
          }
        }
        if (data_packets.length) {
          loadPower = this._getPacketsLoadPower(data_packets);
          result.value = Number((loadPower).toFixed(1)) + 'W';
          result.arrowDeg = loadPower * 1.8;
        }
      } else {
        result.value = Number((data.snapshot_telemetry.current_load * 12).toFixed(1)) + 'W';
        result.arrowDeg = data.snapshot_telemetry.current_load * 18;
      }
    }
    loadPower = (fieldModel == 'daily')
      ? (data_packets.length ? this._getPacketsLoadPower(data_packets) : 0)
      : data.snapshot_telemetry.current_load;

    stateItem = { State__c: -1, Description__c: 'Invalid data' };
    if (loadPower !== undefined && !!window.telemetryStateSettings) {
      stateItem = this._getStateItemForCurrentLoad(loadPower, fieldModel);
    }
    result.state = stateItem.State__c;
    result.description = (this._labelsMock ? this._labelsMock['telemetry_' + (stateItem.Description__c ? stateItem.Description__c.split(' ').join('_') : '')] : '');
    if (result.arrowDeg > 180) {
      result.arrowDeg = 180;
    }
    return result;
  }

  private _getStateItemForCurrentLoad(loadPower: number, fieldModel: string) {
    let stateItem: any = {};
    stateItem = this._getStateByType_find(loadPower, fieldModel, 'loadPower') || { State__c: -1, Description__c: 'Invalid data' };
    return stateItem;
  }

  private _getPacketsLoadPower(packets: Array<any>) {
    let _loadPower: number = 0;
    let _packet: any;
    for (_packet of packets) {
      if (_packet['user_load_power']) {
        _loadPower += +_packet['user_load_power'];
      }
    }

    _loadPower = _loadPower / packets.length;

    return _loadPower;
  }

  /*  <—————————————————————————————————————————————————> *\
                    End Current Load
  \*  <—————————————————————————————————————————————————> */




  /*  <—————————————————————————————————————————————————> *\
                    Start reception
  \*  <—————————————————————————————————————————————————> */



  public getReceptionColor(fieldModel: string, data: any) {

    let reception = (fieldModel == 'daily')
      ? data.no_reception_time
      : data.snapshot_telemetry.rssi;
    let stateItem = { Class_name__c: 'yellow-2', Description__c: 'Invalid data' };

    if (!!window.telemetryStateSettings) {
      stateItem = this._getStateByType_find(reception, fieldModel, 'reception') || { Class_name__c: 'yellow-2', Description__c: 'Invalid data' };
    }

    return {
      value: reception,
      class_name: stateItem.Class_name__c,
      description: (this._labelsMock ?this._labelsMock['telemetry_' + (stateItem.Description__c ? stateItem.Description__c.split(' ').join('_') : '')] : '') || ''
    }
  }

  /*  <—————————————————————————————————————————————————> *\
                    End reception
  \*  <—————————————————————————————————————————————————> */






  /*  <—————————————————————————————————————————————————> *\
                    Start graphs
  \*  <—————————————————————————————————————————————————> */



  private _fillWithoutAllPackets(daily: any) {
    let allDay = [...daily.data_packets];
    let day: Date = new Date(allDay[0].start_time);
    let emptyDay: Array<any> = [];

    for (let i = 0; i < 24; ++i) {
      emptyDay.push({
        start_time: new Date(day.setHours(i)).toISOString(),
        panel_generated_power: null,
        user_load_power: null,
        state_of_charge: null
      });
    }

    let fullDay = emptyDay.map(function(item1) {
      return Object.assign(item1, allDay.find(function(item2) {
        if (Date.parse(item1.start_time) === Date.parse(item2.start_time)) {
          return item2;
        }
      }));
    });

    return this._toConsumableArray(fullDay);
  }

  public getDataForGrapthByDateRange(dateFrom: Date, dateTo: Date) {
    let graphData: Array<any> = [];
    let hoursData = [];
    let x = [];
    this._dws.dailyData.map((_data:any) => {console.log({'telemetry_time':_data.telemetry_time_stamp, 'packets':_data.data_packets})});
    for (let daily of (this._dws.dailyData || [])) {
      if (this._converDateTimeToTS(dateFrom.toISOString()) <= this._converDateTimeToTS(daily.telemetry_time_stamp) &&
        this._converDateTimeToTS(dateTo.toISOString()) >= this._converDateTimeToTS(daily.telemetry_time_stamp)) {
        if (daily.data_packets.length === 24) {
          graphData = [...graphData, ...this._toConsumableArray(daily.data_packets)];
        } else {
          if (daily.data_packets && daily.data_packets.length) {
            graphData = [...graphData, ...this._fillWithoutAllPackets(daily)];
          }
        }
      }
    }

    graphData = graphData.sort((a: any, b: any) => {
      return Date.parse(a.start_time) - Date.parse(b.start_time);
    });

    console.log(graphData);

    for (let hour of graphData) {
      // hoursData.push([new Date(hour.start_time), getRandomArbitrary(1, 100), getRandomArbitrary(1, 100), getRandomArbitrary(1, 100)]);
      hoursData.push([new Date(hour.start_time), hour.panel_generated_power, hour.user_load_power, hour.state_of_charge]);
      x.push(new Date(hour.start_time));
    }

    let period = graphData.length / 8;
    if (period % 1 === 0) {
      x = x.filter(function(_, i) {
        return i % period / 8 == 0;
      });
    } else {
      let firstHour = Date.parse(x[0].toString());
      let lastHour = Date.parse(x[x.length - 1].toString());
      let _period = (lastHour - firstHour) / 8;
      let newX = [];
      for (let i = 1; i < 9; i++) {
        newX.push(new Date(firstHour + _period * i));
      }
      x = newX;
    }
    // vm.hoursData = hoursData;
    // vm.x = x;
    if (x.length === 0 || hoursData.length === 0) {
      x = [dateFrom, dateTo];
      hoursData = [[dateFrom, 0, 0, 0], [dateTo, 0, 0, 0]];
    }
    return { x: x, hoursData: hoursData };
  };


  private _toConsumableArray(arr: any) {
    let i: number = 0;
    let arr2: Array<any> = [];
    if (Array.isArray(arr)) {
      for (i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }
      return arr2;
    } else {
      return Array.from(arr);
    }
  }

  private _converDateTimeToTS(date: string) {
    let b: Array<any> = date.split(/\D+/);
    let newdate = new Date(Date.UTC(b[0], --b[1], b[2], 0, 0, 0, 0));
    return Date.parse(newdate.toString());
  };

  // private _converDateTimeToJsDate(date: string) {
  //   let b: Array<any> = date.split(/\D+/);
  //   let newdate = new Date(b[0], --b[1], b[2], b[3], b[4], b[5], 0);
  //   return newdate;
  // };

  /*  <—————————————————————————————————————————————————> *\
                    End graphs
  \*  <—————————————————————————————————————————————————> */





  /*  <—————————————————————————————————————————————————> *\
                    Start last credit
  \*  <—————————————————————————————————————————————————> */


  public updateLeftCredit(fieldModel: string, data: any) {
    let leftCreditBalance: string = this.getLeftCreditBalance(fieldModel, data.data);
    this.setLeftCreditBalance(leftCreditBalance);
  }

  public get leftCreditBalance(): Observable<string> {
    return this.leftCreditBalanceData.asObservable();
  }

  public setLeftCreditBalance(data: string) {
    this.leftCreditBalanceData.next(data);
  }

  private _getTimeDifferences(timeStr: string):ITimeDifference {
    var now: Date = new Date();
    var comparableTime: Date = new Date(timeStr);
    var diffMs: number = +comparableTime - (+now);
    var timeDifference: ITimeDifference = {
      diffDays: 0,
      diffHrs:  0,
      diffMins: 0
    };
    if (diffMs > 0) {
      timeDifference = {
        diffDays: Math.floor(diffMs / 86400000),
        diffHrs:  Math.floor((diffMs % 86400000) / 3600000),
        diffMins: Math.round(((diffMs % 86400000) % 3600000) / 60000)
      };
    }
    return timeDifference;
  }


  public getLeftCreditBalance(fieldModel: string, data: any): string {
    let res: string = '';
    if (data) {
      if (fieldModel == 'daily') {
        let numdays:    number = Math.floor(data.credit_minutes / 1440);
        let numhours:   number = Math.floor((data.credit_minutes % 1440) / 60);
        let numminutes: number = Math.floor((data.credit_minutes % 1440) % 60);
        if (numdays < 0) {
          numdays = 0;
        }
        if (numhours < 0) {
          numhours = 0;
        }
        if (numminutes < 0) {
          numminutes = 0;
        }
        if (numdays > 100) {
          res = numdays + "+ days ";
        } else {
          res = numdays + " days " + numhours + " hours " + numminutes + " minutes ";
        }
      } else {
        let dtObj: ITimeDifference | number = this._getTimeDifferences(data.snapshot_telemetry.idu_end_time);
        res = dtObj.diffDays + " days " + dtObj.diffHrs + " hours " + dtObj.diffMins + " minutes ";
      }
    }
    return res;
  }


  /*  <—————————————————————————————————————————————————> *\
                    End last credit
  \*  <—————————————————————————————————————————————————> */






  /*  <—————————————————————————————————————————————————> *\
                    Start telemetry
  \*  <—————————————————————————————————————————————————> */


  public get pollingDailyData(): Observable<IDTParams> {
    return this.dailyTelemetryParams.asObservable();
  }
  public get pollingSnapshotData(): Observable<IDTParams> {
    return this.snapshotTelemetryParams.asObservable();
  }

  public get errorsListener(): Observable<{daily:string, snapshot: string}> {
    return this.responseErrorsData.asObservable();
  }

  private _interruptDaily(): void {
    clearInterval(this._pollingTimeout__daily);
    this.IS_INTERRUPTED_POST__DAILY = false;
    this._pollingTime__daily = 0;
    this.dailyTelemetryParams.next({ 
      isPolling: false, 
      pollingTime: '', 
      maxTime: (this.REQUEST_TIMEOUT * this.REQUESTS_AMOUNT), 
      currentTime: 0, 
      procent: 0 
    });
    this.getDashboardDaily();
  }


  private _interruptSnapshot(): void {
    clearInterval(this._pollingTimeout__snapshot);
    this.IS_INTERRUPTED_POST__SNAPSHOT = false;
    this._pollingTime__snapshot = 0;
    this.snapshotTelemetryParams.next({ 
      isPolling: false, 
      pollingTime: '', 
      maxTime: (this.REQUEST_TIMEOUT * this.REQUESTS_AMOUNT), 
      currentTime: 0, 
      procent: 0 
    });
    this.getDashboardSnapshot(this._pollingStartTime_snapshot);
  }

  private getPollingDailyTime(): void {
    let _minutes: number = 0;
    let _seconds: number = 0;
    let _procent: number = 0;
    let _maxTime: number = 0;

    _maxTime = (this.REQUESTS_AMOUNT * this.REQUEST_TIMEOUT);
    _procent = 100 * (this._pollingTime__daily / _maxTime);
    _seconds = this._pollingTime__daily % this.REQUEST_TIMEOUT;
    _minutes = (this._pollingTime__daily - _seconds) / 60;


    let _dpm: IDTParams = {
      isPolling: true,
      pollingTime: '' + _minutes + ':' + _seconds,
      maxTime: _maxTime,
      currentTime: this._pollingTime__daily,
      procent: ~~_procent
    }

    if (_seconds < 10) {
      _dpm.pollingTime = '' + _minutes + ':0' + _seconds
    }

    this.dailyTelemetryParams.next(_dpm);
  }


  private getPollingSnapshotTime(): void {
    let _minutes: number = 0;
    let _seconds: number = 0;
    let _procent: number = 0;
    let _maxTime: number = 0;

    _maxTime = (this.REQUESTS_AMOUNT * this.REQUEST_TIMEOUT);
    _procent = 100 * (this._pollingTime__snapshot / _maxTime);
    _seconds = this._pollingTime__snapshot % this.REQUEST_TIMEOUT;
    _minutes = (this._pollingTime__snapshot - _seconds) / 60;

    let _spm: IDTParams = {
      isPolling: true,
      pollingTime: '' + _minutes + ':' + _seconds,
      maxTime: _maxTime,
      currentTime: this._pollingTime__snapshot,
      procent: ~~_procent
    }

    if (_seconds < 10) {
      _spm.pollingTime = '' + _minutes + ':0' + _seconds
    }

    this.snapshotTelemetryParams.next(_spm);
  }

  public startPollingDaily(): void {

    this._pollingTime__daily = this.REQUEST_TIMEOUT * this.REQUESTS_AMOUNT;
    this._pollingTimeout__daily = setInterval(() => {
      if (0 === this._pollingTime__daily % this.REQUEST_TIMEOUT) {
        this.postDashboardDaily();
      }
      --this._pollingTime__daily;
      this.getPollingDailyTime();
      if (0 === this._pollingTime__daily) {
        this._interruptDaily();
      }

      if (this.IS_INTERRUPTED_POST__DAILY) {
        this._interruptDaily();
        return;
      }
    }, 1000);

  }

  public startPollingSnapshot(): void {

    this._pollingStartTime_snapshot = new Date();
    this._pollingTime__snapshot = this.REQUEST_TIMEOUT * this.REQUESTS_AMOUNT;
    this._pollingTimeout__snapshot = setInterval(() => {
      if (0 === this._pollingTime__snapshot % this.REQUEST_TIMEOUT) {
        this.postDashboardSnapshot();
      }
      --this._pollingTime__snapshot;
      this.getPollingSnapshotTime();
      if (0 === this._pollingTime__snapshot) {
        this._interruptSnapshot();
      }

      if (this.IS_INTERRUPTED_POST__SNAPSHOT) {
        this._interruptSnapshot();
        return;
      }
    }, 1000);

  }


  /*  <—————————————————————————————————————————————————> *\
                    End telemetry
  \*  <—————————————————————————————————————————————————> */



}
