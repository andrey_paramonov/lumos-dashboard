import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {ServerService} from '../server.service';

@Injectable()
export class LabelsResolve implements Resolve<any> {
    constructor(private model: ServerService) { }
    
    resolve(): Observable<any> {
        return this.model.getLabelsData();
    }
}