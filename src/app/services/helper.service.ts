/*
********************************************************************
********************************************************************
***    Helper service for additional functionality
********************************************************************
********************************************************************
*/


import {Injectable} from '@angular/core';
import {Router, NavigationEnd, Event} from '@angular/router';




// **********************************
// ****  Class definition
// **********************************
@Injectable()
export class HelperService {

  private _href: string = '';


  // **********************************
  // ****  Initialization
  // **********************************
  constructor(private router: Router) {
    this.router.events.subscribe((val: Event) => { 
      if (val instanceof NavigationEnd) {
        this.href = window.location.href;
      }
    });
  }





  // **********************************
  // ****  href setter
  // **********************************
  public set href(newHref: string) {
    this._href = newHref;
  }





  // **********************************
  // ****  href getter
  // **********************************
  public get href():string {
    return this._href;
  }






  // **********************************
  // ****  Parsing URL parameters
  // **********************************
  public getURLParameter(name: string):string|null {

    name = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(this.href);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

}