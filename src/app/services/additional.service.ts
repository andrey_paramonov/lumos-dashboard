

import {IPanelC} from '../interfaces/panel-c.interface';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ServerService} from './server.service';

import {LabelsMockupService} from '../mockups/labels.mockup';


interface IAdditionalDetails {
  key: string,
  value: string | number | null
}

// **********************************
// ****  Class definition
// **********************************
@Injectable()
export class AdditionalService {

  private _additionalDetails: Subject<IAdditionalDetails[]> = new Subject();
  private _isPanelC: Subject<IPanelC> = new Subject();

  private _serviceAdditionalDetails: IAdditionalDetails[] = [];

  private _contractId: string = '';
  private _spsId: string = '';

  private _labelsMock: undefined | any;

  constructor(
    private lblsSrv: LabelsMockupService,
    private model: ServerService) {
    this._setLabels();
  }


  public initialization(contractId: string, spsId: string): void {
    this._contractId = contractId;
    this._spsId = spsId;
    this._additionalDetails.subscribe((data: IAdditionalDetails[]) => {
      this._serviceAdditionalDetails = data;
    });
    this._getAdditionalDetailsFromServer();
  }




  // **********************************
  // ****  Set up Labels
  // **********************************
  private _setLabels() {
    this._labelsMock = this.lblsSrv.labels;
    // if (environment.production) {
    //   if (!window.telemetryStateSettings) {
    //     window.telemetryStateSettings = [];
    //   }
    // }
  }


  public get isPanelC(): Observable<IPanelC> {
    return this._isPanelC.asObservable();
  }

  public get additionalDetails(): Observable<IAdditionalDetails[]> {
    return this._additionalDetails.asObservable();
  }

  public setAdditionalDetails(additionalDetails: IAdditionalDetails[]): void {
    this._additionalDetails.next(additionalDetails);
  }


  private _getAdditionalDetailsFromServer():void {

    let newField: IAdditionalDetails;
    let panelC: IPanelC = {
      is_PanelC: false,
      panelC_label: ''
    };
    this.model.getDetails(this._contractId, this._spsId, (response: any) => {
      if (response) {
        panelC.is_PanelC = !!response.is_panelC;
        panelC.panelC_label = response.panelC_label || '';
        this._isPanelC.next(panelC);
        if (response.customer_name) {
          newField = {
            key: this._labelsMock.telemetry_Customer_name,
            value: response.customer_name
          };
          this._updateAdditionalDetails(newField);
        }
        if (response.status) {
          newField = {
            key: this._labelsMock.Status,
            value: response.status
          };
          this._updateAdditionalDetails(newField);
        }
        if (response.sps_id) {
          newField = {
            key: this._labelsMock.SystemID,
            value: response.sps_id
          };
          this._updateAdditionalDetails(newField);
        }
      }
    });
  }


  private _updateAdditionalDetails(data: IAdditionalDetails): void {
    let isExist: boolean = false;
    this._serviceAdditionalDetails.forEach((additionalDetail: IAdditionalDetails) => {
      if (additionalDetail.key === data.key) {
        additionalDetail = data;
        isExist = true;
      }
    });
    if (!isExist) {
      this._serviceAdditionalDetails.push(data);
    }
    this.setAdditionalDetails(this._serviceAdditionalDetails);
  }


  public setIDU(IDU: string): void {
    let newField: IAdditionalDetails = {
      key: this._labelsMock.IDU_version,
      value: IDU
    };
    this._updateAdditionalDetails(newField);
  }

  // **********************************
  // ****  ContractId setter
  // **********************************
  public set contractId(contractId: string) {
    this._contractId = contractId;
  }


  // **********************************
  // ****  ContractId getter
  // **********************************
  public get contractId(): string {
    return this._contractId;
  }


  // **********************************
  // ****  SpsId setter
  // **********************************
  public set spsId(spsId: string) {
    this._spsId = spsId;
  }


  // **********************************
  // ****  SpsId getter
  // **********************************
  public get spsId(): string {
    return this._spsId;
  }




}