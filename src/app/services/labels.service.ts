/*
********************************************************************
********************************************************************
***    Service for recieving $Labels
********************************************************************
********************************************************************
*/

import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';




// **********************************
// ****  Class definition
// **********************************
@Injectable()
export class LabelsService {


  public labelsData:Subject<any> = new Subject();

  constructor() { }


  // **********************************
  // ****  Set labels data 
  // **********************************
  public setLabels(labels: any): void {
    this.labelsData.next(labels);
  } 


  // **********************************
  // ****  Subscribing labels data 
  // **********************************
  public get labels(): Observable<any> {
    return this.labelsData.asObservable();
  } 


}