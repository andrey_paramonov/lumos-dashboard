import {Injectable} from '@angular/core';

declare let google: any;

@Injectable()
export class GoogleChart {

  private xAxis: Array<any> = [];
  private elementId: string = '';
  private chart: any;
  private dataView: any;
  private chartData: any;
  private chartHours: Array<any> = [];

  private columns: Array<any> = [
    {
      type: 'datetime',
      label: 'x'
    },
    {
      type: 'number',
      label: 'Average production',
      color: '#FFC805',
      disabledColor: '#FFD9D9',
      visible: true
    },
    {
      type: 'number',
      label: 'Average consumption',
      color: '#FF0101',
      disabledColor: '#D9D9FF',
      visible: true,
    },
    {
      type: 'number',
      label: 'NSOC',
      color: '#2131FF',
      disabledColor: '#C3E6C3',
      visible: true,
    }
  ];

  constructor() {
    google.charts.load('current', {
      packages: ['corechart', 'line']
    });
  }

  public initChart(elementId: string, xAxis: Array<any>, chartHours: Array<any>) {
    this.elementId = elementId;
    this.xAxis = xAxis;
    this.chartHours = chartHours;

    let drawChart = () => {
      this.chartData = google.visualization.arrayToDataTable([
        ['x', 'Average production', 'Average consumption', 'NSOC'],
        // _chartHours
        ...this.chartHours
      ],
        false
      );
      let date_formatter = new google.visualization.DateFormat({
        pattern: "d.MM.yyyy, HH:mm",
        timeZone: 0
      });
      let wattFormat = new google.visualization.NumberFormat({
        pattern: "# W"
      });
      let percentFormat = new google.visualization.NumberFormat({
        pattern: "# '%'"
      });
      date_formatter.format(this.chartData, 0);
      this.dataView = new google.visualization.DataView(this.chartData);
      this.chart = new google.visualization.LineChart(document.getElementById(this.elementId));
      // Toggle visibility of data series on click of legend.
      google.visualization.events.addListener(this.chart, 'click',  (target: any) => {
        if (target.targetID.match(/^legendentry#\d+$/)) {
          let index = parseInt(target.targetID.slice(12)) + 1;
          this.columns[index].visible = !this.columns[index].visible;
          drawChart();
        }
      });

      let visibleColumnIndexes: Array<any> = [0];
      let colors = [];

      for (let i = 1; i < this.columns.length; i++) {
        if (this.columns[i].visible) {
          colors.push(this.columns[i].color);
          visibleColumnIndexes.push(i);
        } else {
          colors.push(this.columns[i].disabledColor);

          visibleColumnIndexes.push({
            calc: null,
            type: this.columns[i].type,
            label: this.columns[i].label,
          });
        }
      };
      this.dataView.setColumns(visibleColumnIndexes);

      for (let colIndex = 1; colIndex < this.chartData.getNumberOfColumns(); colIndex++) {
        if (colIndex <= 2) {
          wattFormat.format(this.chartData, colIndex);
        } else {
          percentFormat.format(this.chartData, colIndex)
        }
      }
      this.chart.draw(
        this.dataView, {
          interpolateNulls: false,
          height: 400,
          width: '100%',
          tooltip: {
            isHtml: true
          },
          chartArea: {
            width: '90%'
          },
          colors: colors,
          smoothLine: true,
          legend: {
            position: 'bottom'
          },
          focusTarget: 'category',
          series: {
            0: {
              targetAxisIndex: 1
            },
            1: {
              targetAxisIndex: 0
            }
          },

          vAxes: {
            0: {
              title: 'Watt',
              viewWindow: {
                min: 0,
                max: 110
              },
              ticks: [0, 25, 50, 75, 100, 110]

            },
            1: {
              title: '%',
              viewWindow: {
                min: 0,
                max: 110
              },
              ticks: [0, 25, 50, 75, 100, 110]

            },

          },
          hAxis: {
            ticks: this.xAxis,
            format: 'MMM dd, HH:mm',
            timeZone: 0
          }
        }
      );
    }

    google.charts.setOnLoadCallback(drawChart);
  }


}
