export interface ISnapshotDummy {
  volt_panel: number;
  current_panel: number;
  volt_battery: number;
  current_battery: number;
  rssi: number;
  state_of_charge: number;
  nominal_state_of_charge: number;
  state_of_health: number;
  gateway_id: string;
  battery_id: string;
  panel_id: string;
}