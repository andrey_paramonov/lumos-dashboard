export type ISunPanelStatus = '' | 'good' | 'medium' | 'bad';

export interface ISunPanelHealth {
  status: ISunPanelStatus;
  description: string;
  power_value: string;
}
