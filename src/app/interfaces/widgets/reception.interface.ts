export interface IReception {
  class_name: string;
  description: string;
  value: number;
}