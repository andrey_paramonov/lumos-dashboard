export interface IPanelConnectionStatus {
  isConnectionStable: boolean;
  advise: string;
}
