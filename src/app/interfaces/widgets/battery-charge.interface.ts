interface IBatteryInfo {
  battery_level: number;
  description:   string;
  packet_time?:  string;
  soc:           number;
};

interface IChargeInfo {
  charge_level: number;
  description:  string;
  is_charging:  boolean;
  is_steady:    boolean;
}

export interface IBatteryCharge {
  charge_state:     IChargeInfo;
  battery_current:  IBatteryInfo;
  battery_sunrise:  IBatteryInfo;
  battery_sunset:   IBatteryInfo;
  battery_max:      IBatteryInfo;
}
