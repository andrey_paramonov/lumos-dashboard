export interface ICurrentLoad {
  arrowDeg: number;
  description: string;
  fill: string;
  state: number;
  value: string;
}