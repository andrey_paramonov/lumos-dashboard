import {IReception} from './widgets/reception.interface';
import {ICurrentLoad} from './widgets/current-load.interface';
import {IBatteryCharge} from './widgets/battery-charge.interface';
import {ISunPanelHealth} from './widgets/sun-panel-health.interface';
import {IPanelConnectionStatus} from './widgets/panel-connection-status.interface';

export interface IWidgetSettings {
  batteryCharge: IBatteryCharge;
  currentLoad: ICurrentLoad;
  progressBarColor: string;
  dataLoaded: boolean;
  reception: IReception;
  connectionStatus: IPanelConnectionStatus;
  sunPanelHealth: ISunPanelHealth;
}
