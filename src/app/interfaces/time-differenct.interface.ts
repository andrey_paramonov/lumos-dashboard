export interface ITimeDifference {
  diffDays: number;
  diffHrs:  number;
  diffMins: number;
}