export interface IDTParams {
  maxTime: number;
  currentTime: number;
  procent: number;
  isPolling: boolean;
  pollingTime: string;
}