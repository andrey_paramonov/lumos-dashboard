import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {RootComponent} from './root.component';
import {RootRoutingModule} from './root-routing.module';

import {RoundProgressModule, RoundProgressService} from 'angular-svg-round-progressbar';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';

import {ServerService} from './services/server.service';
import {HelperService} from './services/helper.service';
import {AdditionalService} from './services/additional.service';
import {DashboardService} from './services/dashboard.service';
import {LabelsResolve} from './services/resolvers/dashboard-resolve.service';
import {LabelsService} from './services/labels.service';
import {GoogleChart} from './services/google-chart/google-chart.service';
import {LabelsMockupService} from './mockups/labels.mockup';
import {DashboardMockupService} from './mockups/dashboard.mockup';
import {TelemetryStateSettingsMockupService} from './mockups/telemetry-state.mockup';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateCustomParserFormatter} from './services/datepicker-format.service';


@NgModule({
  imports: [
    BrowserModule, 
    RootRoutingModule, 
    CommonModule, 
    RoundProgressModule, 
    HttpClientModule, 
    NgbModule.forRoot(), 
    Ng2GoogleChartsModule
  ],
  declarations: [RootComponent],
  providers: [
    RoundProgressService,
    ServerService,
    AdditionalService,
    HelperService,
    LabelsService,
    LabelsResolve,
    GoogleChart,
    DashboardService,
    LabelsMockupService,
    DashboardMockupService,
    TelemetryStateSettingsMockupService,
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  ],
  bootstrap: [RootComponent],
})
export class RootModule {}