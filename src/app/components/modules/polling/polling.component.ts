import {Component, Input, ChangeDetectorRef, OnChanges} from '@angular/core';
import {IDTParams} from '../../../interfaces/dashboard-telemetry-parameters.interface';
import {DashboardService} from '../../../services/dashboard.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'lum-polling',
  templateUrl: './polling.component.html',
  styleUrls: ['./polling.component.scss']
})
export class PollingComponent implements OnChanges {

  public isPollingButtonDisabled: boolean = false;

  @Input()
  public type!: 'daily' | 'snapshot';

  @Input()
  public parameters!: IDTParams;

  public $Label: any = {};

  constructor(private route: ActivatedRoute, private _dService: DashboardService, private _cd: ChangeDetectorRef) { }

  public ngOnInit(): void {

    // Load labels
    this.route.data.subscribe((data: any) => {
      this.$Label = data.labels;
    });
  }

  public startPolling(): void {

    this.isPollingButtonDisabled = true;
    setTimeout(() => {
      this.isPollingButtonDisabled = false;
    }, 1500);

    if ('daily' === this.type) {
      this._dService.startPollingDaily();
    }

    if ('snapshot' === this.type) {
      this._dService.startPollingSnapshot();
    }
  }

  public ngOnChanges(): void {
    this._cd.markForCheck();
    setTimeout(() => {
      this.isPollingButtonDisabled = false;
      this._cd.markForCheck();
    }, 1500);
  }

}
