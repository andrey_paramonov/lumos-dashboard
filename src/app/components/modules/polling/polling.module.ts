import {NgModule} from '@angular/core';
import {PollingComponent} from './polling.component';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [CommonModule, NgbModule, BrowserAnimationsModule],
  declarations: [PollingComponent],
  exports: [PollingComponent],
})
export class PollingModule {}
