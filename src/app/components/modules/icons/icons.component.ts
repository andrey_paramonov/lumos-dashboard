import {Component, Input} from '@angular/core';

@Component({
  selector: 'lumos-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss'],
})
export class IconsComponent {
  @Input()
  public name!: string;
  @Input()
  public style: string = '';
  @Input()
  public class: string = '';
}
