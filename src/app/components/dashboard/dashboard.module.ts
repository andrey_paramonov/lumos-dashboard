import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard.component';
import {CommonModule} from '@angular/common';
import {GraphsTabModule} from './graphs-tab/graphs-tab.module';
import {GagesTabModule} from './gages-tab/gages-tab.module';
import {BitTabModule} from './bit-tab/bit-tab.module';


@NgModule({
  imports: [FormsModule, CommonModule, GraphsTabModule, GagesTabModule, BitTabModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
})
export class DashboardModule {}
