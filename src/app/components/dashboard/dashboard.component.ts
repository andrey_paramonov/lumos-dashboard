import {DashboardService} from '../../services/dashboard.service';
import {AdditionalService} from '../../services/additional.service';
import {IPanelC} from '../../interfaces/panel-c.interface';
import {Component, ChangeDetectorRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'lum-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  public tabIndex: number = 1;
  public dashboardData: {dailyData: null|any, snapshotData: null|any} = {
    dailyData: null,
    snapshotData: null
  };

  public additionalDetails: any[] = [];

  public $Label: any = {};
  public leftCredit: string = '';

  public errorData: string = '';

  public gagesTabIndex: number|undefined;
  public graphSelectedFrom: Date = new Date();
  public graphSelectedTo: Date = new Date();

  public panelC: IPanelC = {
    is_PanelC: false,
    panelC_label: ''
  };

  public changeGagesTabIndex(gagesTabIndex: number): void {
    this.gagesTabIndex = gagesTabIndex;
  }

  public changeGraphFrom(dateFrom: Date):void {
    this.graphSelectedFrom = dateFrom;
  }

  public changeGraphTo(dateTo: Date):void {
    this.graphSelectedTo = dateTo;
  }

  private isDserviceStarted: boolean = false;
  private isAserviceStarted: boolean = false;



  constructor(
    private route: ActivatedRoute,
    private _dService: DashboardService,
    private _aService: AdditionalService,
    private _cd: ChangeDetectorRef) {
    this.graphSelectedFrom.setDate(this.graphSelectedFrom.getDate() - 3);
    // Default initialization
    this._init();
  }

  private _init(): void {

    this._aService.additionalDetails.subscribe((data: any[]) => {
      this.additionalDetails = data;
      this._cd.detectChanges();
    });

    // Load labels
    this.route.data.subscribe((data: any) => {
      this.$Label = data.labels;

      this._aService.isPanelC.subscribe((panelC: IPanelC) => {
        this.panelC = panelC;
        this.isAserviceStarted = true;
        if (this.isDserviceStarted && this.isAserviceStarted) {
          this._loadData();
        }
      });
      // We have to init only once the service
      this._dService.onDSStart.subscribe(() => {
        console.log('%cInited service!', 'color:#d11;font-weight:bold;font-size:125%;text-decoration:underline;');
        this.isDserviceStarted = true;
        if (this.isDserviceStarted && this.isAserviceStarted) {
          this._loadData();
        }
      });
    });

  }

  private _loadData(): void {
    // Subscribing current data
    this._dService.getDashboardData.subscribe((dashboardData: {dailyData: null|any, snapshotData: null|any}) => {
      this.dashboardData = dashboardData;
      this._cd.detectChanges();
    });

    this._dService.leftCreditBalance.subscribe((leftCredit: string) => {
      this.leftCredit = leftCredit;
    });

    this._dService.errorsListener.subscribe((errorsData: {daily: string, snapshot: string}) => {
      this.errorData = '';
      if ('' !== errorsData.daily) {
        this.errorData = errorsData.daily;
      }
      if ('' !== errorsData.snapshot) {
        this.errorData = errorsData.snapshot;
      }
      console.log('%cErrors info:', 'color:red;text-decoration:underline;');
      console.log(errorsData);
    });


    // if (!this.panelC.is_PanelC) {
      this._dService.getDashboardDaily();
      this._dService.getDashboardSnapshot();
      this._dService.postDashboardDaily();
      this._dService.postDashboardSnapshot();
    // }

    this._cd.detectChanges();
  }

  public onTabChange(index: number): void {
    this.tabIndex = index;
  }

}
