import {Component, OnInit, OnChanges, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef, SimpleChanges} from '@angular/core';
import {IDTParams} from '../../../interfaces/dashboard-telemetry-parameters.interface';
import {IPanelC} from '../../../interfaces/panel-c.interface';
import {WidgetSettings} from '../../../classes/widget-settings.class';
import {DashboardService} from '../../../services/dashboard.service';
import {ActivatedRoute} from '@angular/router';



interface _IDayData {
  label: string;
  data: any;
  isToday: boolean;
  isSnapshot: boolean;
  class: string;
}



@Component({
  selector: 'dashboard-gages',
  templateUrl: './gages-tab.component.html',
  styleUrls: ['./gages-tab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GagesTabComponent implements OnInit, OnChanges {

  public $Label: any = {};

  public tabIndex: number = 3;
  public days: Array<_IDayData> = [];

  public widgetSettings: WidgetSettings = new WidgetSettings();

  public todayDashboardData: any = null;
  public selectedFormat: string = 'daily';
  public isTodaySelected: boolean = true;

  public pollingVisibile: boolean = false;

  public pollingDataDaily: IDTParams = {
    isPolling: false,
    pollingTime: '',
    currentTime: 0,
    maxTime: 0,
    procent: 0
  };
  public pollingDataSnapshot: IDTParams = {
    isPolling: false,
    pollingTime: '',
    currentTime: 0,
    maxTime: 0,
    procent: 0
  };

  @Input()
  public panelC: IPanelC = {
    is_PanelC: false,
    panelC_label: ''
  };


  @Input()
  public gagesTabIndex!: number;

  @Output()
  public gagesTabIndexChange: EventEmitter<number> = new EventEmitter();

  @Input()
  public dashboardData: {dailyData: null|any, snapshotData: null|any} = {
    dailyData: null,
    snapshotData: null
  };

  @Input()
  public errorMessage: string = '';

  public constructor(
    private route: ActivatedRoute,
    private _dService: DashboardService, 
    private _cd: ChangeDetectorRef) {}

  public ngOnInit() {

    this._dService.pollingDailyData.subscribe((data: IDTParams) => {
      this.pollingDataDaily = data;
      this.pollingVisibile = true;
      this._cd.markForCheck();
    });

    this._dService.pollingSnapshotData.subscribe((data: IDTParams) => {
      this.pollingDataSnapshot = data;
      this.pollingVisibile = true;
      this._cd.markForCheck();
    });

    setTimeout(() => {
      if (!this.pollingVisibile) {
        this.pollingVisibile = true;
        this._cd.markForCheck();
      }
    }, 1200);

    // Load labels
    this.route.data.subscribe((data: any) => {
      this.$Label = data.labels;
    });
  }

  public onTabChange(event: {index: number}, isPreselected?: boolean): void {

    this.selectedFormat = 'daily';
    this.tabIndex = event.index;
    this.gagesTabIndex = event.index;
    if (!isPreselected) {
      this.gagesTabIndexChange.emit(event.index);
    }

    if (this.days.length) {
      if (this.days[event.index].isSnapshot) {
        this.selectedFormat = 'snapshot';
      }
      this.isTodaySelected = !!(this.selectedFormat == 'daily' && this.days[event.index].isToday);

      this.widgetSettings = this._dService.getWidgetData(this.selectedFormat, this.days[event.index]);
    }
    this._cd.detectChanges();
    this._cd.markForCheck();

  }

  public ngOnChanges(changes: SimpleChanges): void {

    let hasDaily: boolean = false;

    this.days = [];

    if (this.dashboardData.dailyData) {
      this.processDaily();
      hasDaily = true;
    } 

    if (!hasDaily) {
      this._checkDaysExistingToday();
    }

    this._pushEmptySnapshot();

    if (this.dashboardData.snapshotData) {
      this.processSnapshot();
    }

    this._cd.detectChanges();
    if (changes.dashboardData) {
      this._selectDefaultTab();
    } else {
      this.onTabChange({index: this.tabIndex}, true);
    }
  }

  private _selectDefaultTab(): void {

    if ('daily' === this.selectedFormat) {
      this.tabIndex = this.days.length-2;
      this._cd.detectChanges();
    }

    if (this.gagesTabIndex && this.tabIndex != this.gagesTabIndex && this.gagesTabIndex < this.days.length) {
      this.tabIndex = this.gagesTabIndex;
      this._cd.detectChanges();
    }

    if ('snapshot' === this.selectedFormat) {
      this.tabIndex = this.days.length-1;
      this._cd.detectChanges();
    } 

    this._cd.markForCheck();

    this.onTabChange({index: this.tabIndex}, true);
  }


  private sortDailyData(): void {
    this.dashboardData.dailyData.sort((a: any, b: any) => {
      return (new Date(a.telemetry_time_stamp).getTime()) - (new Date(b.telemetry_time_stamp).getTime());
    })
  }

  public processDaily(): void {

    if (this.dashboardData.dailyData) {
      this._clearData();
      this.sortDailyData();
      this.dashboardData.dailyData.forEach((dayData: any) => {
        let _dd: _IDayData = {
          label: '',
          data: null,
          isToday: false,
          isSnapshot: false,
          class: ''
        };
        if (!this._dService.isTodayDate(dayData.telemetry_time_stamp)) {
          _dd.label = dayData.telemetry_time_stamp;
          _dd.data = dayData;
          this.days.push(_dd);
        } else {
          this.todayDashboardData = dayData;
        }
      });

      this._checkDaysExistingToday();
      this._cutData();
    }

  }

  private _cutData(): void {
    if (this.days.length > 9) {
      this.days = this.days.slice(-9);
    }
  }

  private _clearData(): void {
    this.todayDashboardData = null;
    this.days = [];
  }


  private _checkDaysExistingToday(): void {
    let _dd: _IDayData = {
      label: 'Today',
      data: this.todayDashboardData,
      isToday: true,
      isSnapshot: false,
      class: ' today'
    };
    this.days.push(_dd);
  }


  private _pushEmptySnapshot(): void {
    let _dd: _IDayData = {
      label: 'Snapshot',
      data: null,
      isToday: false,
      isSnapshot: true,
      class: ' snapshot'
    };
    this.days.push(_dd);
  }

  public processSnapshot(): void {
    if (this.dashboardData.snapshotData) {
      if (this.days.length) {
        this.days[this.days.length-1].data = this.dashboardData.snapshotData[0];
        this._cd.detectChanges();
      }
    }
  }

}
