import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {GagesTabComponent} from './gages-tab.component';
import {DropdownModule} from 'primeng/dropdown';
import {GrowlModule} from 'primeng/growl';
import {ChartModule} from 'primeng/chart';
import {TabViewModule} from 'primeng/tabview';
import {IconsModule} from '../../modules/icons/icons.module';
import {PollingModule} from '../../modules/polling/polling.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {RoundProgressModule, RoundProgressService} from 'angular-svg-round-progressbar';

import {WidgetsTemplateModule} from './widgets-template/widgets-template.module';

import {FromIframePipeModule} from '../../pipes/from-iframe.module';

@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    GrowlModule,
    ChartModule,
    IconsModule,
    TabViewModule,
    RoundProgressModule,
    BrowserAnimationsModule,
    WidgetsTemplateModule,
    PollingModule,
    FromIframePipeModule
    ],
  providers: [RoundProgressService],
  declarations: [GagesTabComponent],
  exports: [GagesTabComponent],
})
export class GagesTabModule {}