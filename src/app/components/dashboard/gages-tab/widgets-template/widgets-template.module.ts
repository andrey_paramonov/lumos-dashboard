import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {WidgetsTemplateComponent} from './widgets-template.component';
import {DropdownModule} from 'primeng/dropdown';
import {GrowlModule} from 'primeng/growl';
import {ChartModule} from 'primeng/chart';
import {TabViewModule} from 'primeng/tabview';
import {IconsModule} from '../../../modules/icons/icons.module';
import {BatteryModule} from './battery/battery.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {RoundProgressModule, RoundProgressService} from 'angular-svg-round-progressbar';

@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    GrowlModule,
    ChartModule,
    IconsModule,
    TabViewModule,
    RoundProgressModule,
    BrowserAnimationsModule,
    BatteryModule
    ],
  providers: [RoundProgressService],
  declarations: [WidgetsTemplateComponent],
  exports: [WidgetsTemplateComponent],
})
export class WidgetsTemplateModule {}