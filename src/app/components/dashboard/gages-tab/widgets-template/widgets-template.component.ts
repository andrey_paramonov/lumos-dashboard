import {Component, Input, OnInit, OnChanges, ChangeDetectorRef} from '@angular/core';
import {WidgetSettings} from '../../../../classes/widget-settings.class';
import {EColors} from '../../../../enums/colors.enums';
import {DashboardService} from '../../../../services/dashboard.service';
import {ActivatedRoute} from '@angular/router';
import {IPanelC} from '../../../../interfaces/panel-c.interface';

@Component({
  selector: 'widgets-template',
  templateUrl: './widgets-template.component.html',
  styleUrls: ['./widgets-template.component.scss'],
})
export class WidgetsTemplateComponent implements OnInit, OnChanges {

  @Input()
  public panelC: IPanelC = {
    is_PanelC: false,
    panelC_label: ''
  };

  @Input()
  public widgetSettings: WidgetSettings = new WidgetSettings();
  @Input()
  public selectedFormat: string = 'daily';

  public $Label: any = {};

  public _progressBarColor: string = '';
  public _progressBarCurrent: number = 0;

  public lightgreyColor: string = EColors.LIGHTGREY;

  public constructor(
    private _cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private _dService: DashboardService) {}


  public ngOnInit() {
    
    // Load labels
    this.route.data.subscribe((data: any) => {
      this.$Label = data.labels;
    });
  }

  public get sunPanelClass(): string {

    if (!this.widgetSettings.dataLoaded) {
      return '';
    }

    let _spClass: string = '';

    if ('good' === this.widgetSettings.sunPanelHealth.status) {
      _spClass = 'sun-panel__good';
    }
    if ('medium' === this.widgetSettings.sunPanelHealth.status) {
      _spClass = 'sun-panel__medium';
    }
    if ('bad' === this.widgetSettings.sunPanelHealth.status) {
      _spClass = 'sun-panel__bad';
    }

    return _spClass;
  }

  public get isSteady(): boolean {
    let isSteady: boolean = false;

    isSteady = (this.widgetSettings.dataLoaded && this.widgetSettings.batteryCharge.charge_state.is_steady);
    return isSteady;
  }

  public get widgetColor(): string {
    let color: string = EColors.LIGHTGREY;
    if (this.widgetSettings.dataLoaded) {
      if (this.widgetSettings.batteryCharge.charge_state.is_steady) {
        color = EColors.YELLOW;
      } else if (this.widgetSettings.batteryCharge.charge_state.charge_level !== 0) {
        if (this.widgetSettings.batteryCharge.charge_state.is_charging) {
          color = EColors.GREEN;
        } else {
          color = EColors.RED;
        }
      }
    }
    return color; 
  }


  public getBatteryPartColor(batteryPart: number, batteryLevel: number): string {
    return this._dService.getBatteryPartColor(batteryPart, batteryLevel);
  }


  public calcAngle(num: number): any {
    return {transform: 'rotate('+num+'deg)'};
  }

  public loadColor(): string {
    let color: string = '';
    switch (this.widgetSettings.currentLoad.state) {
      case 1: color = EColors.BLUE;
        break;
      case 2: color = EColors.GREEN;
        break;
      case 3: color = EColors.YELLOW;
        break;
      case 4: color = EColors.RED;
        break;
      default: color = EColors.BLUE;
        break;
    }
    return color;
  }

  public calcLoading(): number {
    let params: any = {
      max: 100,
      blue: 2.4,
      green: 60,
      yellow: 97.2
    };
    let loading: number = 50;
    switch (this.widgetSettings.currentLoad.state) {
      case 1: loading = (50/params.max) * params.blue;
        break;
      case 2: loading = (50/params.max) * params.green;
        break;
      case 3:  loading = (50/params.max) * params.yellow;
        break;
      case 4: loading = 50;
        break;
      default: loading = (50/params.max) * params.blue;
        break;
    }
    return loading;
  }


  public ngOnChanges(): void {

    if (this.widgetSettings) {
      this._progressBarColor = this.loadColor();
      this._progressBarCurrent = this.calcLoading();
      // this.widgetSettings.progressBarColor = this.widgetSettings.currentLoad.fill;
    }
    this._cd.detectChanges();
  }

}
