import {Component, Input} from '@angular/core';
import {DashboardService} from '../../../../../services/dashboard.service';

@Component({
  selector: 'battery',
  templateUrl: './battery.component.html',
  styleUrls: ['./battery.component.scss'],
})
export class BatteryComponent {

  @Input()
  public batteryName: string = '';
  @Input()
  public titleClass: string = '';
  @Input()
  public Label: string = '';
  @Input()
  public batteryLevel: number = -1;

  public constructor(private _dService: DashboardService) {}


  public getBatteryPartColor(batteryPart: number): string {
    return this._dService.getBatteryPartColor(batteryPart, this.batteryLevel);
  }


  public get batteryHeight(): string {
    return 'linear-gradient(to top, #c2c2c2 0%,  #c2c2c2 85%, #f3f3f3 85%, transparent 85%';
  }


}
