import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BatteryComponent} from './battery.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BatteryComponent],
  exports: [BatteryComponent],
})
export class BatteryModule {}
