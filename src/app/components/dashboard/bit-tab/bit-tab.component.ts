import {Component, Input, OnChanges} from '@angular/core';
import {IDTParams} from '../../../interfaces/dashboard-telemetry-parameters.interface';
import {IPanelC} from '../../../interfaces/panel-c.interface';
import {DashboardService} from '../../../services/dashboard.service';
import {SnapshotDummy} from '../../../classes/snapshot-dummy.class';

@Component({
  selector: 'dashboard-bit',
  templateUrl: './bit-tab.component.html',
  styleUrls: ['./bit-tab.component.scss'],
})
export class BitTabComponent implements OnChanges {
  
  @Input()
  public panelC: IPanelC = {
    is_PanelC: false,
    panelC_label: ''
  };

  @Input()
  public snapshotData: null | Array<any> = null;

  @Input()
  public errorMessage: string = '';

  public componentData: any = new SnapshotDummy;

  public contentVisible: boolean = false;

  public pollingDataSnapshot: IDTParams = {
    isPolling: false,
    pollingTime: '',
    currentTime: 0,
    maxTime: 0,
    procent: 0
  };

  public constructor(private _dService: DashboardService) {

    this._dService.pollingSnapshotData.subscribe((data: IDTParams) => {
      this.pollingDataSnapshot = data;
      this.contentVisible = true;
    });

    setTimeout(() => {
      if (!this.contentVisible) {
        this.contentVisible = true;
      }
    }, 1200);
  }

  public ngOnChanges(): void {
    if (this.snapshotData !== undefined && this.snapshotData !== null && this.snapshotData.length && this.snapshotData[0].snapshot_telemetry) {
      this.componentData = this.snapshotData[0].snapshot_telemetry;
    } else {
      this.componentData = new SnapshotDummy;
    }
  }

}
