import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BitTabComponent} from './bit-tab.component';
import {PollingModule} from '../../modules/polling/polling.module';

import {FromIframePipeModule} from '../../pipes/from-iframe.module';

@NgModule({
  imports: [CommonModule, PollingModule, FromIframePipeModule],
  declarations: [BitTabComponent],
  exports: [BitTabComponent],
})
export class BitTabModule {}