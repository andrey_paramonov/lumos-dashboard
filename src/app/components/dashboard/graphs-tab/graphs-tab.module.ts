import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {GraphsTabComponent} from './graphs-tab.component';
import {DropdownModule} from 'primeng/dropdown';
import {GrowlModule} from 'primeng/growl';
import {ChartModule} from 'primeng/chart';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FromIframePipeModule} from '../../pipes/from-iframe.module';

@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    GrowlModule,
    ChartModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    FormsModule,
    AngularFontAwesomeModule,
    FromIframePipeModule
    ],
  declarations: [GraphsTabComponent],
  exports: [GraphsTabComponent],
})
export class GraphsTabModule {}