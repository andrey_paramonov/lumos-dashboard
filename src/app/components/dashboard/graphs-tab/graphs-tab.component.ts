import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {DashboardService} from '../../../services/dashboard.service';
import {IPanelC} from '../../../interfaces/panel-c.interface';
import {GoogleChart} from '../../../services/google-chart/google-chart.service';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'dashboard-graphs',
  templateUrl: './graphs-tab.component.html',
  styleUrls: ['./graphs-tab.component.scss'],
})
export class GraphsTabComponent implements OnInit {
  
  public fromModel!: NgbDateStruct;
  public toModel!: NgbDateStruct;

  public minDate!: NgbDateStruct;
  public maxDate!: NgbDateStruct;

  public toDate: Date = new Date();
  public fromDate: Date = new Date();

  @Input()
  public panelC: IPanelC = {
    is_PanelC: false,
    panelC_label: ''
  };

  @Input()
  public selectedFromDate!: Date;
  @Input()
  public selectedToDate!: Date;

  @Input()
  public errorMessage: string = '';

  @Output()
  public changeDateFrom: EventEmitter<Date> = new EventEmitter();

  @Output()
  public changeDateTo: EventEmitter<Date> = new EventEmitter();

  public constructor(private _dService: DashboardService, private _gService: GoogleChart) {
    
  }

  public initGraphsData(): void {

    this.fromModel = {
      year: this.fromDate.getFullYear(),
      month: this.fromDate.getMonth()+1,
      day: this.fromDate.getDate()
    };
    this.toModel = {
      year: this.toDate.getFullYear(),
      month: this.toDate.getMonth()+1,
      day: this.toDate.getDate()
    };

    let _minDate: Date = new Date();
    _minDate.setDate(_minDate.getDate()-60);
    this.minDate = {
      year: _minDate.getFullYear(),
      month: _minDate.getMonth()+1,
      day: _minDate.getDate()
    };
    this.maxDate = {
      year: this.toDate.getFullYear(),
      month: this.toDate.getMonth()+1,
      day: this.toDate.getDate()
    };
  }

  public ngOnInit() {
    this.fromDate = this.selectedFromDate;
    this.toDate = this.selectedToDate;
    this.initGraphsData();
    
    this.loadGraphData();
  }

  private loadGraphData(): void {
    let graphData = this._dService.getDataForGrapthByDateRange(this.fromDate, this.toDate);
    this._gService.initChart('googleChart', graphData.x, graphData.hoursData);
  }

  public onDateSelect(selectedDate: NgbDateStruct, type: 'from' | 'to'): void {
    if ('from' === type) {
      this.fromDate.setDate(selectedDate.day);
      this.fromDate.setMonth(selectedDate.month - 1);
      this.fromDate.setFullYear(selectedDate.year);
      this.changeDateFrom.emit(this.fromDate);
    }
    if ('to' === type) {
      this.toDate.setDate(selectedDate.day);
      this.toDate.setMonth(selectedDate.month - 1);
      this.toDate.setFullYear(selectedDate.year);
      this.changeDateTo.emit(this.toDate);
    }
    this.loadGraphData();
  }
}
