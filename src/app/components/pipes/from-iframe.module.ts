
import {FromIframePipe} from './from-iframe.pipe';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [CommonModule],
  declarations: [FromIframePipe],
  exports: [FromIframePipe],
})
export class FromIframePipeModule {}