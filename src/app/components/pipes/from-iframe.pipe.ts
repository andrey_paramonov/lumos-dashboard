import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'fromIframe' })
export class FromIframePipe implements PipeTransform {
  constructor() {}
  transform(iframeContent: string) {
    let _bodyContent: string = iframeContent;
    let _REresult: RegExpMatchArray | null = iframeContent.match(/(\<body((?!\>).)*\>)(.*)(\<\/body\>)/gm);
    if (_REresult !== null && _REresult.length) {
      _bodyContent = _REresult[0];
    }
    _bodyContent = _bodyContent.replace(/↵/gm, '');
    return _bodyContent;
  }
} 