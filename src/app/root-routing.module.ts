import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LabelsResolve} from './services/resolvers/dashboard-resolve.service';
import {DashboardComponent} from './components/dashboard/dashboard.component';

const appRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve: {
      labels: LabelsResolve
    }
  },
  // {path: '**', component: NotFoundPageComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { useHash: true }
    ),
  ],
  exports: [RouterModule],
  providers: [],
})
export class RootRoutingModule {}
