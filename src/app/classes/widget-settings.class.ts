import {IWidgetSettings} from '../interfaces/widget-settings.interface';
import {ICurrentLoad} from '../interfaces/widgets/current-load.interface';
import {IReception} from '../interfaces/widgets/reception.interface';
import {IBatteryCharge} from '../interfaces/widgets/battery-charge.interface';
import {ISunPanelHealth} from '../interfaces/widgets/sun-panel-health.interface';
import {IPanelConnectionStatus} from '../interfaces/widgets/panel-connection-status.interface';

export class WidgetSettings implements IWidgetSettings {
  public batteryCharge: IBatteryCharge = {
    charge_state: {
      charge_level: 0,
      description:  '',
      is_charging:  false,
      is_steady:    false
    },
    battery_current: {
      battery_level: 0,
      description: '',
      soc: 0
    },
    battery_sunrise: {
      battery_level: 0,
      description: '',
      soc: 0
    },
    battery_sunset: {
      battery_level: 0,
      description: '',
      soc: 0
    },
    battery_max: {
      battery_level: 0,
      description: '',
      soc: 0
    }
  };
  public reception: IReception = {
    class_name: 'yellow-2',
    description: '',
    value: 0
  };
  public currentLoad: ICurrentLoad = {
    arrowDeg: 0,
    description: '',
    fill: '#EDF0F6',
    state: 1,
    value: '0W'
  };
  public progressBarColor: string = '#F8CF1C';
  public dataLoaded: boolean = false;
  public connectionStatus: IPanelConnectionStatus = {
    isConnectionStable: false,
    advise: ''
  }
  public sunPanelHealth: ISunPanelHealth = {
    status: '',
    description: '',
    power_value: ''
  };
}

