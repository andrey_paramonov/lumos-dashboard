import {ISnapshotDummy} from '../interfaces/snapshot-dummy.interface';

export class SnapshotDummy implements ISnapshotDummy {

  public volt_panel: number = 0;
  public current_panel: number = 0;
  public volt_battery: number = 0;
  public current_battery: number = 0;
  public rssi: number = 0;
  public state_of_charge: number = 0;
  public nominal_state_of_charge: number = 0;
  public state_of_health: number = 0;
  public gateway_id: string = '';
  public battery_id: string = '';
  public panel_id: string = '';
}