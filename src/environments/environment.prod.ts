import {EnvironmentData} from './environment-data';

declare const resourceUrlPrefix: string;

export const environment: EnvironmentData = {
  production: true,
  resourceUrlPrefix,
};
