export interface EnvironmentData {
  production: boolean;
  resourceUrlPrefix: string;
}
