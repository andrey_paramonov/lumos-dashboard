# Lumos

This project was generated with
[Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app
will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can
also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the
`dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via
[Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via
[Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Api

For debug add parameter `&d=1`

### Contracts list

Request path `/contracts?s=<search string>`

```json
{

	success: true,
	data: [
		{
			contractId: '725432426',
			accountId: '894856165',
			customerName: 'Evgenii Stok',
			createdDate: {
				human: '04/07/1995',
				unix: 188758732
			},
			phoneNumber: '+3804944984',
			spsId: '08498484',
			active: true
		}
	]
}
```

```json
{

	success: false,
	errors: [
		'We got some error here'
	]
}
```

### Contracts by account

Request path `/account?id=<accountId>`

```
{
	user: {
		accountId: '894856165',
		name: 'Konstantin Valeshkovsky',
		phoneNumber: '+380631234567',
		email: 'longusername@longusernameservice.com',
		type: 'Payer',
		country: 'Ukraine',
		street: '10 Armeyskaya st.'
	},
	contracts: [
		{
	        contractId: '129012987',
	        msdism: '2332231564',
	        status: 'Complete',
	        type: 'Production',
	        offer: 'CI Soft Launch',
	        seniority: '190 (days)',
	        city: 'Abidjan',
	        street: '10 Armeyskaya st.',
	        role: 'Main Payer',

			activityLogs: [
		      {
			      type: 'SMS',
			      text: 'Line Type: SMS'
		      },
		      {
		      	type: 'Activation',
		      	text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'
		      },
		    ],

		    cases: [
		      {
		      	creationDate: {
		      		human: '09/05/2011',
		      		unix: 13498468420
		      	},
		      	number:'1235427',
		      	status: 'New',
		      	type: 'Manage Debt',
		      	subtype: 'Change Debt'
		      },
		      {
		      	creationDate: {
		      		human: '09/05/2011',
		      		unix: 13498468420
		      	},
		      	number:'6234363',
		      	status: 'New',
		      	type: 'Manage Debt',
		      	subtype: 'Change Debt'
		      }
		    ],

			assets: [
		      {
		      	name: 'B',
		      	group: 'SPS',
		      	description: ''
		      },
		      {
		      	name: '1',
		      	group: 'Rate Plan',
		      	description: ''
		      }
		    ],

		    operations: [
		      {
		        name: 'Operation #1',
		        description: 'Some description for operation #1',
		        steps: [
		          { label: 'Op#1 Step 1', status:'Done' },
		          { label: 'Op#1 Step 2', status:'Error' },
		          { label: 'Op#1 Step 3', status:'Warning' },
		          { label: 'Op#1 Step 4', status:'Active' },
		          { label: 'Op#1 Step 5', status:'Done' }
		        ]
		      },
		      {
		        name: 'Operation #2',
		        description: 'Some description for operation #2',
		        steps: [
		          { label: 'Op#2 Step 1', status:'Error' },
		          { label: 'Op#2 Step 2', status:'Active' },
		          { label: 'Op#2 Step 3', status:'Warning' },
		          { label: 'Op#2 Step 4', status:'' },
		          { label: 'Op#2 Step 5', status:'Done' }
		        ]
		      }
		    ],

		    sps: {
		    	spsId: '08498484',
		    	status: 'Installed',
		    	paymentStatus: 'Paid',
		    	activeEnd: {
		    		human: '2018-01-05 12:04:21',
		    		unix: 1515153861
		    	},
		    	creditBalance: 234,
		    	seniority: 324,
		    	...
		    }
	    },

	    {
	        contractId: '129114983',
	        msdism: '8345123453',
	        status: 'Pending',
	        type: 'Sandbox',
	        offer: 'CI Soft Launch',
	        seniority: '150 (days)',
	        city: 'Odessa',
	        street: '10 Armeyskaya st.',
	        role: 'Secondary Payer',

			activityLogs: [
		      {
			      type: 'SMS',
			      text: 'Line Type: SMS'
		      },
		      {
		      	type: 'Activation',
		      	text: 'Activated Set: IDU: 124902; Panel: 000000000000000080045b2 Deactivated Set'
		      },
		    ],

		    cases: [
		      {
		      	creationDate: {
		      		human: '09/05/2011',
		      		unix: 13498468420
		      	},
		      	number:'1235427',
		      	status: 'New',
		      	type: 'Manage Debt',
		      	subtype: 'Change Debt'
		      },
		      {
		      	creationDate: {
		      		human: '09/05/2011',
		      		unix: 13498468420
		      	},
		      	number:'6234363',
		      	status: 'New',
		      	type: 'Manage Debt',
		      	subtype: 'Change Debt'
		      }
		    ],

			assets: [
		      {
		      	name: 'B',
		      	group: 'SPS',
		      	description: ''
		      },
		      {
		      	name: '1',
		      	group: 'Rate Plan',
		      	description: ''
		      }
		    ],

		    operations: [
		      {
		        name: 'Operation #1',
		        description: 'Some description for operation #1',
		        steps: [
		          { label: 'Op#1 Step 1', status:'Done' },
		          { label: 'Op#1 Step 2', status:'Error' },
		          { label: 'Op#1 Step 3', status:'Warning' },
		          { label: 'Op#1 Step 4', status:'Active' },
		          { label: 'Op#1 Step 5', status:'Done' }
		        ]
		      },
		      {
		        name: 'Operation #2',
		        description: 'Some description for operation #2',
		        steps: [
		          { label: 'Op#2 Step 1', status:'Error' },
		          { label: 'Op#2 Step 2', status:'Active' },
		          { label: 'Op#2 Step 3', status:'Warning' },
		          { label: 'Op#2 Step 4', status:'' },
		          { label: 'Op#2 Step 5', status:'Done' }
		        ]
		      }
		    ]
	    },
	    ...
	],

	success: true

}
```
